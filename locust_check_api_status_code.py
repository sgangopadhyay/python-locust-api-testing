from locust import HttpUser
from locust import task
from locust import between


class Suman(HttpUser):
    url = "https://650fc5963ce5d181df5ca945.mockapi.io/automation_testing"
    wait_time = between(5, 15)

    @task
    def api_usr_status_code(self):
        response = self.client.get(self.url)
        if response.status_code != 200:
            print(f"Unexpected status code: {response.status_code}")
