from locust import HttpUser
from locust import task


class SumanApiTestingUsingLocust(HttpUser):

    @task
    def first_test(self):
        self.client.get("/")


url = "https://650fc5963ce5d181df5ca945.mockapi.io/automation_testing"
