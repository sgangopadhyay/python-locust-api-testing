# Name : Suman Gangopadhyay
# Email : suman@guvi.in / linuxgurusuman@gmail.com
# Date : 12-Oct-2023
# About : Python Locust Load Testing of RestAPI Server 
# Gitlab URL : https://gitlab.com/sgangopadhyay/python-locust-api-testing

# RUN A LOCUST API TEST

## Normal Usage with a locustfile.conf

```
locust
```

## Running the Locust API Test with a master.conf File 

```
locust --config=locust.conf
```

## Running Locust on specific Host URL i.e. 127.0.0.1

```
locust --web-host 127.0.0.1
```

## Running a LOCUST.CONF on a Specific Host 127.0.0.1

```
locust --config=locust.conf --web-host 127.0.0.1
```

